/**
 * Namespace-class for [pixi.js](http://www.pixijs.com/).
 *
 * Contains assorted static properties and enumerations.
 *
 * @namespace PIXI
 * @author Mat Groves http://matgroves.com/ @Doormat23
 */
var PIXI = PIXI || {};
var hello = 'hello';
hello = '1';
hello = '1';hello = '1';hello = '1';hello = '1';hello = '1';

var test = hello;
hello = test;